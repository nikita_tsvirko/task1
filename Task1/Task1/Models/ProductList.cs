﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task1.Models
{
    public class ProductList
    {
        public List<Product> Products;

        public ProductList()
        {
            Products = new List<Product>();
        }
    }
}
