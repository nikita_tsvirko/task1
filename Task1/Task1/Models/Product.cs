﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task1.Models
{
    public class Product
    {
        public string Name { get; set; }
        public string Info { get; set; }
        public string Date { get; set; }
        public double Price { get; set; }
    }
}