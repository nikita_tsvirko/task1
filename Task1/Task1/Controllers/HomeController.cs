﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task1.Models;

namespace Task1.Controllers
{
    public class HomeController : Controller
    {
        static ProductList productList = new ProductList();
        
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Index(Product product)
        {
            productList.Products.Add(product);
            return View("IndexPost");
        }
        
        public IActionResult List()
        {
            return View(productList.Products.ToList());
        }
    }
}